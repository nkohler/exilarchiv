# American Guild - Network Data

**Projekt im Rahmen des Hackathons *Coding Da Vinci* 2020**

## Inhalt und Hintergrund des Projekts

Der vom Deutschen Exilarchiv 1933-1945 bereitgestellte Datensatz enthält 
Informationen zu Briefwechseln von Intellektuellen, die zur Zeit des 
Nationalsozialismus aus Deutschland fliehen mussten, wie Thomas Mann oder Stefan 
Zweig. All diese Personen standen in Kontakt mit der "American Guild for German 
Cultural Freedom", die teilweise lebenswichtige Unterstützung bot.      

Mit unserer Netzwerkdarstellung arbeiten wir bisher nicht erschlossene 
Zusammenhänge der im Exilarchiv gelagerten Personenakten heraus. Durch die 
zusätzliche Visualisierung auf einer Weltkarte wird eindringlich klar, was 
"Exil" bedeutet. Dies soll BesucherInnen der Dauerausstellung "Deutschen 
Exilarchiv 1933-1945" der Deutschen Nationalbibliothek (DNB) eine neue 
*Erfahrbarkeit* der Daten ermöglichen. Durch eine interaktive Benutzeroberfläche 
können Interessierte selbst einstellen, welche Aspekte der Daten dargestellt 
werden. Somit lässt sich beispielsweise nachvollziehen, wer mit wem 
kommunizierte und wie intensiv dieser Kontakt war. Die Aufbereitung der Daten 
in ein *erforschbares* Format dient nicht nur unserer Visualisierung, sondern 
macht die Informationen auch für wissenschaftliche Zwecke zugänglicher. Somit 
helfen unsere Vorarbeiten der DNB, gemäß ihrer strategischen Ausrichtung, Werke 
benutzerfreundlich zu erschließen.      

Spannend war für uns alle der Aspekt, mit einem großen Datensatz zu arbeiten, 
diesen mit Linked-Open-Data anzureichern, sowie die Zusammenhänge durch 
interaktive Grafiken wie Netzwerkanalysen darzustellen. Technisch umgesetzt 
wurde das Projekt durch Datensäuberung in R und anschließende Visualisierung 
mit visNetwork in R und plotly in Python. Der entstandene Code ist auf GitLab 
und das Netzwerk zusätzlich als shiny Webapp verfügbar.

## Das haben wir erreicht

- Visualisierung der Kommunikation zwischen Unterschiedlichen Personen mit Link zu DNB
- Visualisierung der Orte, von denen aus Briefe geschrieben wurden
- Aufbereitung des Datensatzes

## Zukunftsperspektiven

- Ausarbeitung des Prototyps (Anreicherung der Daten, Vergrößerung der Interaktionsmöglichkeiten mit Visualisierungen)
- Gamification der Daten für Ausstellungsbesucher
- Gestenerkennung zur Steuerung der Benutzeroberfläche für Ausstellungsbesucher

## Struktur des Datensatzes
### `AmericanGuild_with_linked_data.csv`

| Spalte | Bezeichnung | Betrifft | Bedeutung | Beispiel |
|-----------|-------------|----------|-----------|----------|
| 1 | **hierarchie_id** | Dokument | Interne Hierarchieebene (Exilarchiv) | `'D.01.01.0001'` |
| 2 | **idn** | Dokument | Identifikationsnummer des Dokuments. Kann zur Recherche in der DNB verwendet werden. | `'1028660502'` |
| 3 | **typ** | Dokument | Typus des Dokuments | `'Brief'` / `'Gutachten'` |
| 4 | **umfang** | Dokument | Inhalt, Art, Umfang der Daten | `'1 Blatt handschriftlich mit Unterschrift'` |
| 5 | **fussnote** | Dokument | Zusatzinformation zum Dokument | `'Betrifft: Preisausschreiben'` |
| 6 | **sprache** | Dokument | Im Dokument verwendete Sprache | `'Schrift, Sprache: Englisch'` |
| 7 | **material_id** | Dokument | Identifikationsnummer des verwendeten Materials. Kann zur Recherche in der DNB verwendet werden. | `'41466098'` |
| 8 | **entstehung_jahr** | Dokument | Jahr der Erstellung des Dokuments | `'1938'` |
| 9 | **entstehung_datum** | Dokument | Genaues Datum der Erstellung des Dokuments | `'20.05.1938'` |
| 10 | **entstehung_ort** | Dokument | Herkunftsort | `'[New York, NY]'` ( *[ ]* Verweist auf unsichere Informationen zur Ortsangabe) |
| 11 | **entstehung_land** | Dokument | Herkunftsland | Keine Rücksichtnahme auf Sicherheit der Angabe in *entstehung_ort* |
| 12 | **sender_id** | Dokument (Verfasser:in) | Absender Identifikationsnummer | `'116004851'` |
| 13 | **sender_name** | Dokument (Verfasser:in) | Absender Name | `'Adolphe Abter'` |
| 14 | **sender_geschlecht** | Dokument (Verfasser:in) | Absender Geschlecht | `'Frau'` / `'Mann'` |
| 15 | **sender_dokumentiert_als** | Dokument (Verfasser:in) | Absender *Rolle* | `'Verfasser'` (ausschließlich) |
| 16 | **sender_qualitaet** | nicht eindeutig | Interne Kürzel bezüglich Archivalien, z.B. Pässe, andere Lebensdokumente oder Quellen, die die Angaben verifizieren | `'Tp1'` / `'Tn'` (veraltet) = weniger qualifizierte Informationen (eigentlich nicht mehr verwendet) |
| 17 | **sender_personenmappe_exist** | Dokument (Verfasser:in) | Gibt an, ob eine Personenakte zu Sender:in existiert | `TRUE` / `FALSE` |
| 18 | **sender_gutachtennmappe_exist** | Dokument (Verfasser:in) | Gibt an, ob eine Gutachtenmappe zu Sender:in existiert | `TRUE` / `FALSE` |
| 19 | **sender_bild** | Dokument (Verfasser:in) | URL zu einem Bild der Person | `'https://commons.wikimedia.org/wiki/Special:FilePath/Elisabeth%20Schumann.jpg?width=270'` |
| 20 | **empf_id** | Dokument (dokumentierte Person) | Empfänger ID | `'116004851'` (mehrere Werte mögl., mit ` \| ` getrennt) |
| 21 | **empf_name** | Dokument (dokumentierte Person) | Empfänger Name | `'Adolphe Abter'` |
| 22 | **empf_geschlecht** | Dokument (dokumentierte Person) | Empfänger Geschlecht | `'Frau'` / `'Mann'` |
| 23 | **empf_dokumentiert_als** | Dokument (dokumentierte Person) | Empfänger *Rolle* | (nicht eindeutig) `'Adressat'` = Dokument war an Person adressiert; `'Dokumentierte Person'` = lediglich dokumentiert, nicht unbedingt Empfänger. Kann auch `'Verfasser'` oder `'Schreiber'` beinhalten. Mehrere Werte mögl., mit ` \| ` getrennt. |
| 24 | **empf_qualitaet** | nicht eindeutig | Interne Kürzel bezüglich Archivalien, z.B. Pässe, andere Lebensdokumente oder Quellen, die die Angaben verifizieren | `'Tp1'` / `'Tn'` (veraltet) = weniger qualifizierte Informationen (eigentlich nicht mehr verwendet) |
| 25 | **empf_personenmappe_exist** | Dokument (dokumentierte Person) | Gibt an, ob eine Personenakte zu Empfänger:in existiert | `TRUE` / `FALSE` |
| 26 | **empf_gutachtenmappe_exist** | Dokument (dokumentierte Person) | Gibt an, ob eine Gutachtenmappe zu Empfänger:in existiert | `TRUE` / `FALSE` |
| 27 | **empf_bild** | Dokument (dokumentierte Person) | URL zu einem Bild der Person | `'https://commons.wikimedia.org/wiki/Special:FilePath/Elisabeth%20Schumann.jpg?width=270'` |
| 28 | **akte_name** | Personenakte (aus der das Dokument stammt) | Name der Person, zu der die Personenakte gehört | `'Adolphe Abter'` |
| 29 | **akte_typ** | Personenakte (aus der das Dokument stammt) | Gehört das Dokument zu einer Personenmappe oder zu einer sich darin befindlichen Gutachtenmappe? | `'Personenmappe'` / `'Gutachtenmappe'` |
| 30 | **akte_rolle** | Personenakte (aus der das Dokument stammt) | Rolle der in Personenakte vermerkten Person innerhalb der American Guild | `'Stipendiat'` / `'Gutachter'` |